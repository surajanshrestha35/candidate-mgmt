import { describe, expect, it } from "vitest";
import request from "supertest";
import app from "../../src/server";

describe("Candidate API Tests", () => {
  it("should create a new candidate", async () => {
    const res = await request(app).post("/api/v1/candidates").send({
      firstName: "John",
      lastName: "Doe",
      email: "john.doe@example.com",
      comment: "Experienced full-stack developer.",
      phoneNumber: "+977-9812345678",
      linkedInProfile: "https://linkedin.com/in/johndoe",
      githubProfile: "https://github.com/johndoe",
      callTimeStart: "09:00",
      callTimeEnd: "18:00",
    });
    expect(res.statusCode).toEqual(200);
  });
});
