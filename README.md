# IMP Message 💡: Why I chose Gitlab over Github?

Although my goto service is **Github,** I chose **Gitlab** because my current company monitors my activities on Github and I don't want them to know that I'm applying to other companies.

I'm super active on **Github** and you can check out my contributions there.
Thanks for understanding.

My Github: [https://github.com/SurajanShrestha/](https://github.com/SurajanShrestha/)

## How to run?

1. Make sure you have Docker and Docker Compose installed.
   I recommend installing [Docker Desktop](https://www.docker.com/products/docker-desktop/) as it comes pre-installed with docker engine, docker, docker compose and any other tools needed.
2. Open Docker Desktop
3. Clone this project
4. In root directory, run: `docker compose up --build`
   1. This will not only start the server but also runs `npm test` which runs unit tests and also seeds the database with data 💾.
5. If successful ✅, you can see:
   1. Backend server at endpoint: `http://localhost:8080/api/v1`
   2. PostgreSQL Database at: `5432` port
6. If failure ❌, and you already have setup Docker then:
   1. Make sure Docker Desktop is installed correctly
   2. If you have PostgreSQL already installed in your local machine, sometimes default Ports can collide.
      Solution 👇:
      1. If you already have a local database connection at PORT `5432` , then you need to close that port.

## Tech Used

1. Language: Typescript
2. Server: Nodejs, Expressjs
3. Database: PostgreSQL
4. ORM: Sequelize
