import { DataTypes } from "sequelize";
import sequelize from "@/database";
import { validations } from "@/utils";
import { CandidateInstance, ICandidate } from "@/dtos";

const {
  isRequired,
  length,
  isValidEmail,
  isValidPhoneNumber,
  isValidTime,
  isValidUrl,
} = validations;

export const Candidate = sequelize.define<CandidateInstance>("Candidate", {
  firstName: {
    type: DataTypes.STRING,
    allowNull: false,
    validate: {
      ...isRequired("First Name"),
      ...length(1, 50),
    },
  },
  lastName: {
    type: DataTypes.STRING,
    allowNull: false,
    validate: {
      ...isRequired("Last Name"),
      ...length(1, 50),
    },
  },
  phoneNumber: {
    type: DataTypes.STRING,
    validate: {
      ...length(1, 20),
      ...isValidPhoneNumber,
    },
  },
  email: {
    type: DataTypes.STRING,
    allowNull: false,
    unique: true,
    validate: {
      ...isRequired("Email"),
      ...isValidEmail,
      ...length(5, 100),
    },
  },
  callTimeStart: {
    type: DataTypes.TIME,
    validate: isValidTime,
  },
  callTimeEnd: {
    type: DataTypes.TIME,
    validate: isValidTime,
  },
  linkedInUrl: {
    type: DataTypes.STRING,
    validate: {
      ...isValidUrl,
      ...length(0, 255),
    },
  },
  githubUrl: {
    type: DataTypes.STRING,
    validate: {
      ...isValidUrl,
      ...length(0, 255),
    },
  },
  comment: {
    type: DataTypes.TEXT("long"),
    allowNull: false,
    validate: {
      ...isRequired("Comment"),
    },
  },
});

// Note 👉: Replace empty strings with "null".
Candidate.beforeValidate((candidate: CandidateInstance) => {
  type IOptionals = keyof ICandidate;
  const optionals: IOptionals[] = [
    "phoneNumber",
    "callTimeStart",
    "callTimeEnd",
    "linkedInUrl",
    "githubUrl",
  ];
  optionals.forEach((field) => {
    const value = candidate.get(field as keyof CandidateInstance);
    if (value === "") {
      candidate.set(field, null);
    }
  });
});

// Note 👉: Update the candidate which already exists (found through email).
Candidate.beforeCreate(async (candidate: CandidateInstance) => {
  const existingCandidate = await Candidate.findOne({
    where: { email: candidate.email },
  });
  if (existingCandidate) {
    await existingCandidate.update(candidate.toJSON());
    throw new Error("EXISTING_CANDIDATE_UPDATED");
  }
});
