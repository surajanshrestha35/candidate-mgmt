// import { ICandidateCreation } from "../../dtos";
// import { ICandidateRepository } from "./candidate.repository";

// Note 👉: NoSQL ODM model. Eg: Model imported from mongoose for MongoDB
// import { NoSqlCandidateModel } from "../../models/nosql";

// Note 👉: If we're using a nosql database instead of sql.
// export class NoSQLCandidateRepository implements ICandidateRepository {
//   async create(payload: ICandidateCreation) {
//     const candidate = await NoSqlCandidateModel.create(payload);
//     return candidate;
//   }
//   async findByEmail(email: string) {
//     const candidate = await NoSqlCandidateModel.findOne({ where: { email: email } });
//     return candidate;
//   }
// }
