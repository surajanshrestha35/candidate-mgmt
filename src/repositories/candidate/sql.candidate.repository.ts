import { ICandidateCreation } from "../../dtos";
import { ICandidateRepository } from "./candidate.repository";
import { Candidate } from "../../models";

export class SQLCandidateRepository implements ICandidateRepository {
  async create(payload: ICandidateCreation) {
    const candidate = await Candidate.create(payload);
    return candidate;
  }
  async findByEmail(email: string) {
    const candidate = await Candidate.findOne({ where: { email: email } });
    return candidate;
  }
  async getAll() {
    const candidates = await Candidate.findAll();
    return candidates;
  }
}
