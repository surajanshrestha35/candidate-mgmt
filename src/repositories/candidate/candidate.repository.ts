import { CandidateInstance, ICandidate, ICandidateCreation } from "../../dtos";

export interface ICandidateRepository {
  create(candidate: ICandidateCreation): Promise<CandidateInstance>;
  findByEmail(email: string): Promise<ICandidate | null>;
  getAll(): Promise<CandidateInstance[]>;
}
