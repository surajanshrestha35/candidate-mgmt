// import { config } from "../config";
import { SQLCandidateRepository } from "./candidate";

// Note 👉: Now, we can easily swap between SQL and NoSQL databases with very little configuration.
export class RepositoryFactory {
  static getCandidateRepository() {
    // Note 💡:  If database is "nosql", use NoSqlCandidateRepository containing logic for NoSQL ODM's like mongoose (If used MongoDB as a nosql database).
    // if (config.database === "nosql") {
    //   return new NoSqlCandidateRepository();
    // }
    return new SQLCandidateRepository();
  }
}
