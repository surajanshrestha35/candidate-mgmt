import { Request, Response } from "express";
import { handleError } from "@/utils";
import { CandidateService } from "@/services";

const candidateService = new CandidateService();

export const createCandidate = async (req: Request, res: Response) => {
  try {
    console.log(`${req.method}: ${req.path} ${new Date().toString()}`);
    const candidate = await candidateService.createCandidate(req.body);
    console.log(
      "**Candidate successfully created with content: " + req.body.content
    );
    res.json({ message: "Candidate successfully created", data: candidate });
  } catch (err) {
    if (err instanceof Error && err.message === "EXISTING_CANDIDATE_UPDATED") {
      const updatedCandidate = await candidateService.findOneCandidate(
        req.body.email
      );
      res.status(200).json({
        message: "Existing Candidate Updated",
        data: updatedCandidate,
      });
    } else handleError(err, res, "Candidate");
  }
};

export const getAllCandidates = async (req: Request, res: Response) => {
  try {
    console.log(`${req.method}: ${req.path} ${new Date().toString()}`);
    const candidates = await candidateService.getAllCandidates();
    console.log("**Candidates successfully fetched**");
    res.json(candidates);
  } catch (err) {
    console.log(`**Candidates could not be fetched. Error: ${err}`);
    res.status(404).json({
      message: `Something went wrong. Candidates could not be fetched`,
    });
  }
};
