import { ICandidateCreation } from "@/dtos";
import { RepositoryFactory } from "@/repositories";
import { ICandidateRepository } from "@/repositories/candidate";

export class CandidateService {
  private candidateRepository: ICandidateRepository;
  constructor() {
    this.candidateRepository = RepositoryFactory.getCandidateRepository();
  }

  async createCandidate(payload: ICandidateCreation) {
    return this.candidateRepository.create(payload);
  }

  async findOneCandidate(email: string) {
    return this.candidateRepository.findByEmail(email);
  }

  async getAllCandidates() {
    return this.candidateRepository.getAll();
  }
}
