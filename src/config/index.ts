interface IConfig {
  database: "sql" | "nosql";
}

const dbType = process.env.DB_TYPE;

export const config: IConfig = {
  database: dbType === "sql" || dbType === "nosql" ? dbType : "sql",
};
