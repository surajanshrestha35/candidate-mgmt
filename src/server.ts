import cors from "cors";
import "dotenv/config";
import express from "express";
import bodyParser from "body-parser";
import { syncAllModels, testDbConnection } from "@/init";
import { router } from "@/routes";

const port = process.env.PORT || 3000;

const app = express();

// Enable CORS for all origins for now. Later, we can change it to accept only certain domains.
app.use(cors());

// Parse json payload
app.use(bodyParser.json());

// Routes
app.use("/api/v1", router);

// Start Server
app.listen(port, () => {
  console.log(`App listening on port: ${port}`);
});

// DB Connection
testDbConnection();

// Sync all models
syncAllModels(true);

export default app;
