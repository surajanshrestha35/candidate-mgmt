import { Router } from "express";
import { router as candidateRoutes } from "./candidate.routes";

export const router = Router();

const routes = [{ path: "/candidates", route: candidateRoutes }];

routes.forEach((r) => router.use(r.path, r.route));
