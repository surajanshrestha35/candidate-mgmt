import { candidateController } from "@/controllers";

import { Router } from "express";

export const router = Router();

router
  .route("/")
  .get(candidateController.getAllCandidates)
  .post(candidateController.createCandidate);
