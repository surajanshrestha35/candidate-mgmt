import { Model } from "sequelize";

export interface ICandidate {
  firstName: string;
  lastName: string;
  phoneNumber: string | null;
  email: string;
  callTimeStart: Date | null;
  callTimeEnd: Date | null;
  linkedInUrl: string | null;
  githubUrl: string | null;
  comment: string;
}

export interface ICandidateCreation {}

export interface CandidateInstance
  extends Model<ICandidate, ICandidateCreation>,
    ICandidate {
  id: number;
  createdAt?: Date;
  updatedAt?: Date;
}
