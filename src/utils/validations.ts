import { ModelValidateOptions } from "sequelize";

export const validations = {
  isRequired: (field: string): ModelValidateOptions => ({
    notNull: { msg: `${field} is required` },
    notEmpty: { msg: `${field} cannot be empty` },
  }),
  length: (min: number, max: number): ModelValidateOptions => ({
    len: {
      args: [min, max],
      msg: `Must be between ${min} and ${max} characters long`,
    },
  }),
  isValidUrl: {
    isUrl: { msg: "Must be a valid URL" },
  } as ModelValidateOptions,
  isValidEmail: {
    isEmail: { msg: "Must be a valid email address" },
  } as ModelValidateOptions,
  isValidPhoneNumber: {
    is: {
      args: /^[+]?[\d\s()-]+$/,
      msg: "Phone number format is invalid",
    },
  } as ModelValidateOptions,
  isValidTime: {
    isValidTime(value: any) {
      if (value && !/^([01]?[0-9]|2[0-3]):[0-5][0-9]$/.test(value)) {
        throw new Error("Invalid time format. Use HH:MM format.");
      }
    },
  } as ModelValidateOptions,
};
