import { Response } from "express";
import { ValidationError } from "sequelize";

export const handleError = (err: any, res: Response, modelName: string) => {
  if (err instanceof ValidationError) {
    const validationErrors = err.errors.map((err) => {
      const message = err.message.split(": ").pop() || err.message;
      return {
        field: err.path,
        message: message,
      };
    });
    res.status(400).json({ errors: validationErrors });
  } else {
    console.log(`**${modelName} could not be created. Error: ${err}`);
    res.status(404).json({
      message: `Something went wrong. ${modelName} could not be created`,
    });
  }
};
