import sequelize from "@/database";

export const testDbConnection = async () => {
  try {
    await sequelize.authenticate();
    console.log("Database Connection has established successfully");
  } catch (err) {
    console.error("Unable to connect to database: " + err);
  }
};

export const syncAllModels = async (alter: boolean) => {
  try {
    await sequelize.sync({ alter });
    console.log("||*****All Models were synchronized successfully*****||");
  } catch (err) {
    console.log(`Unable to synchronize All Models in the database: ${err}`);
  }
};
